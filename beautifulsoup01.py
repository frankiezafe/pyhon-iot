# see README.md for dependencies
'''
for page rendered with javascript, we need a 'headless' browser to render the page
'''
from bs4 import BeautifulSoup
from selenium import webdriver

url="https://www.flipkart.com/hp-pentium-quad-core-4-gb-1-tb-hdd-dos-15-be010tu-notebook/product-reviews/itmeprzhy4hs4akv?page1&pid=COMEPRZBAPXN2SNF"

browser = webdriver.Chrome()
browser.get(url)
html_source = browser.page_source
browser.quit()

soup = BeautifulSoup(html_source, "html.parser")

print( soup.prettify() )

print( soup.title.string )

for link in soup.find_all('a'):
	print(link.get('href'))

for div in soup.find_all('div',{'class':'content'}):
	divc = link.get('class')
	print( '####################################' )
	#print(div.encode_contents())
	print(div.contents)