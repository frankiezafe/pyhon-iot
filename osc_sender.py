# sudo -H python3 -m pip install setuptools
# sudo -H python3 -m pip install python-osc

import argparse
import random
import time
from pythonosc import osc_message_builder
from pythonosc import udp_client

oscsender = udp_client.SimpleUDPClient("192.168.1.100", 23000)

while True:
	oscsender.send_message("/random", random.random())
	time.sleep(1)