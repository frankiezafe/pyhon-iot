'''
pour lancer un programme dans le server X (interface graphique)
$ export DISPLAY=:0
source: https://superuser.com/questions/1035787/open-a-movie-with-vlc-from-command-line-via-ssh
'''
# sudo -H python3 -m pip install opencv-python
# sudo apt install python3-opencv
# https://www.geeksforgeeks.org/webcam-motion-detector-python/
# Pyhton program to implement 
# WebCam Motion Detector 

# importing OpenCV, time and Pandas library 
import cv2, time 
# importing datetime class from datetime library 
from datetime import datetime 

BLUR = 21
THRESHOLD = 90
MIN_AREA = 4000
REFRESH_BACK_RATE = -1
SHOW_IMAGES = True
UNPACK_COUNT = 2 # for rapsberry, 2 for regular linux distro
ANALYSIS_RESOLUTION = (320,240)

# Assigning our static_back to None 
static_back = None

# Capturing video 
video = cv2.VideoCapture(0)

frame_count = 0

# Infinite while loop to treat stack of image as video 
while True: 
	# Reading frame(image) from video 
	check, frame = video.read() 
	
	low_res = cv2.resize(frame, ANALYSIS_RESOLUTION)
	
	frame_count += 1
	
	# Converting color image to gray_scale image 
	gray = cv2.cvtColor(low_res, cv2.COLOR_BGR2GRAY) 

	# Converting gray scale image to GaussianBlur 
	# so that change can be find easily 
	gray = cv2.GaussianBlur(gray, (BLUR, BLUR), 0) 

	# In first iteration we assign the value 
	# of static_back to our first frame 
	if static_back is None or ( REFRESH_BACK_RATE != -1 and frame_count % REFRESH_BACK_RATE == 0): 
		static_back = gray
		continue

	# Difference between static background 
	# and current frame(which is GaussianBlur) 
	diff_frame = cv2.absdiff(static_back, gray) 

	# If change in between static background and 
	# current frame is greater than 30 it will show white color(255) 
	thresh_frame = cv2.threshold(diff_frame, THRESHOLD, 255, cv2.THRESH_BINARY)[1] 
	thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2) 

	# Finding contour of moving object
	cnts = None
	if UNPACK_COUNT == 3:
		(_, cnts, _) = cv2.findContours( thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	elif UNPACK_COUNT == 2:
		(cnts, _) = cv2.findContours( thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	
	contour_num = 0
	for contour in cnts: 
		if cv2.contourArea(contour) < MIN_AREA: 
			continue
		(x, y, w, h) = cv2.boundingRect(contour) 
		# making green rectangle arround the moving object 
		cv2.rectangle(low_res, (x, y), (x + w, y + h), (0, 255, 0), 3) 
		contour_num += 1
	print( contour_num )

	if SHOW_IMAGES:
		# Displaying image in gray_scale 
		cv2.imshow("Gray Frame", gray) 
		# Displaying the difference in currentframe to 
		# the staticframe(very first_frame) 
		cv2.imshow("Difference Frame", diff_frame) 
		# Displaying the black and white image in which if 
		# intencity difference greater than 30 it will appear white 
		cv2.imshow("Threshold Frame", thresh_frame) 
		# Displaying color frame with contour of motion of object 
		cv2.imshow("Color Frame", low_res)
	
	key = cv2.waitKey(1) 
	# if q entered whole process will stop 
	if key == ord('q'): 
		# if something is movingthen it append the end time of movement 
		if motion == 1: 
			time.append(datetime.now()) 
		break

video.release()

# Destroying all the windows 
cv2.destroyAllWindows()
