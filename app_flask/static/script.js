function up() {
  fetch("http://amelie.local:5000/up/");
}

function down() {
  fetch("http://amelie.local:5000/down/");
}

function display_value() {
  fetch('http://amelie.local:5000/get_value/')
  .then((resp) => resp.json()) // Transform the data into json
  .then(function(data) {
    //console.log( data );
    //console.log( JSON.stringify(data) );
    document.getElementById('servo_value').innerHTML = JSON.stringify(data);
  })
}
window.setInterval( display_value, 200 );