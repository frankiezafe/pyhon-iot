from flask import Flask, render_template, jsonify
import random
from time import sleep

app = Flask(__name__)

value = random.random() * 180
last_valid_value = value

@app.route('/')
def index():
    return render_template('index.html', value=value)

@app.route('/up/')
def up():
    global last_valid_value
    last_valid_value += 5
    last_valid_value %= 180
    #setAngle(last_valid_value)
    return jsonify(last_valid_value)

@app.route('/down/')
def down():
    global last_valid_value
    last_valid_value -= 5
    while last_valid_value < 0:
        last_valid_value += 180
    #setAngle(last_valid_value)
    return jsonify(last_valid_value)

@app.route('/get_value/')
def get_value():
    return jsonify(last_valid_value)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

