'''
pour lancer un programme dans le server X (interface graphique)
$ export DISPLAY=:0
source: https://superuser.com/questions/1035787/open-a-movie-with-vlc-from-command-line-via-ssh
'''
# sudo -H python3 -m pip install opencv-python
# sudo apt install python3-opencv
# sudo -H python3 -m pip install setuptools
# sudo -H python3 -m pip install python-osc
# https://www.geeksforgeeks.org/webcam-motion-detector-python/
# Pyhton program to implement 
# WebCam Motion Detector 

from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2, time, io, copy, argparse, random
import numpy as np
from pythonosc import osc_message_builder
from pythonosc import udp_client

BLUR = 21
THRESHOLD = 45
MIN_AREA = 800
REFRESH_BACK_RATE = -1
SHOW_IMAGES = True
UNPACK_COUNT = 3 # for rapsberry, 2 for regular linux distro
ANALYSIS_RESOLUTION = (320,240)

OSC_PORT = 23000

OSCclients = []
for i in range(2,256):
	c = {
		'ip' : "10.3.14."+str(i) + ":" + str(OSC_PORT),
		'osc' : udp_client.SimpleUDPClient("10.3.14."+str(i), OSC_PORT),
		'active' : True
	}
	OSCclients.append( c )
# discovering active OSCclients
for c in OSCclients:
	try:
		c['osc'].send_message("/hello", random.random())
	except:
		print( "Failed to contact " + c['ip'] )
		c['active'] = False

camera = PiCamera()
camera.resolution = ANALYSIS_RESOLUTION
#camera.exposure_mode = 'night'
#camera.exposure_mode = 'off'
#camera.drc_strength='high'
#camera.drc_strength='off'
#camera.awb_mode = 'off'
#camera.awb_gains = 6
camera.framerate = 25
rawCapture = PiRGBArray(camera, size=ANALYSIS_RESOLUTION)

time.sleep(0.1)

frame_count = 0
static_back = None

# Infinite while loop to treat stack of image as video 
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	
	low_res = copy.copy(rawCapture.array)
	rawCapture.truncate(0)
	
	frame_count += 1
	
	if frame_count == 50:
		camera.exposure_mode = 'off'
		camera.drc_strength='off'
		camera.awb_mode = 'off'
	elif frame_count < 50:
		continue
	
	# Converting color image to gray_scale image 
	gray = cv2.cvtColor(low_res, cv2.COLOR_BGR2GRAY)

	# Converting gray scale image to GaussianBlur 
	# so that change can be find easily 
	gray = cv2.GaussianBlur(gray, (BLUR, BLUR), 0) 

	# In first iteration we assign the value 
	# of static_back to our first frame 
	if static_back is None or ( REFRESH_BACK_RATE != -1 and frame_count % REFRESH_BACK_RATE == 0): 
		static_back = gray
		continue

	# Difference between static background 
	# and current frame(which is GaussianBlur) 
	diff_frame = cv2.absdiff(static_back, gray) 

	# If change in between static background and 
	# current frame is greater than 30 it will show white color(255) 
	thresh_frame = cv2.threshold(diff_frame, THRESHOLD, 255, cv2.THRESH_BINARY)[1] 
	thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2) 

	# Finding contour of moving object
	cnts = None
	if UNPACK_COUNT == 3:
		(_, cnts, _) = cv2.findContours( thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	elif UNPACK_COUNT == 2:
		(cnts, _) = cv2.findContours( thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE )
	
	contour_num = 0
	contour_info = []
	for contour in cnts: 
		if cv2.contourArea(contour) < MIN_AREA: 
			continue
		(x, y, w, h) = cv2.boundingRect(contour) 
		# making green rectangle arround the moving object 
		cv2.rectangle(low_res, (x, y), (x + w, y + h), (0, 255, 0), 3)
		
		contour_info.append( x + w / 2 )
		contour_info.append( y + h / 2 )
		contour_info.append( w )
		contour_info.append( h )
		
		contour_num += 1
	#print( contour_num )
	
	contour_info = [contour_num] + contour_info
	for c in OSCclients:
		if not c['active']:
			continue
		c['osc'].send_message("/blob", contour_info )

	if SHOW_IMAGES:
		# Displaying image in gray_scale 
		cv2.imshow("Gray Frame", gray) 
		# Displaying the difference in currentframe to 
		# the staticframe(very first_frame) 
		cv2.imshow("Difference Frame", diff_frame) 
		# Displaying the black and white image in which if 
		# intencity difference greater than 30 it will appear white 
		cv2.imshow("Threshold Frame", thresh_frame) 
		# Displaying color frame with contour of motion of object 
		cv2.imshow("Color Frame", low_res)
	
	key = cv2.waitKey(1) 
	# if q entered whole process will stop 
	if key == ord('q'): 
		# if something is movingthen it append the end time of movement 
		if motion == 1: 
			time.append(datetime.now()) 
		break

# Destroying all the windows 
cv2.destroyAllWindows()
