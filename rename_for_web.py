import os

FOLDER = 'html/images'

for item in os.listdir(FOLDER):
	webname = item.lower().replace(' ', '_')
	print( webname, '>', item )
	os.rename(os.path.join(FOLDER,item), os.path.join(FOLDER,webname))