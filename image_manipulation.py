# sudo python3 -m pip install pillow
from PIL import Image

# resizing / thumbnailing
'''
im = Image.open("html/images/colored_fence.jpg")
#im.thumbnail((50,50), Image.ANTIALIAS)
im = im.resize((50,50))
im.save("html/images/colored_fence_thumb50x50.jpg")
'''

# new image
'''
im = Image.new("RGB", (800, 600), "white")
src = Image.open("html/images/colored_fence.jpg")
src = src.resize( (300,300) )
im.paste(src, (20,20))
im.paste(src, (80,70))
src = Image.open("html/images/no_stopping_sign.jpg")
src = src.resize( (300,300) )
im.paste(src, (120,400))
im.save("html/images/white.jpg")
'''
# access pixels
im = Image.open("html/images/colored_fence.jpg")
im = im.resize((200,200))
im2 = Image.open("html/images/no_stopping_sign.jpg")
im2 = im2.resize((200,200))
iw, ih = im.size
#im.show()
#inpu = input()
pixels = im.load()
pixels2= im2.load()
last_r = 0
for y in range(0,ih):
	for x in range(0,iw):
		r, g, b = pixels[x,y]
		# channel blending
		r2, g2, b2 = pixels2[x,y]
		pixels[x,y] = ( r2-r, g, b )
		# symetry/mirror
		'''
		pixels[(iw-1) - x,y] = pixels[x,y]
		''''
		# conditional red inversion
		'''
		if last_r > 127:
			pixels[x,y] = ( 255-r, g, b )
		last_r = r
		'''
		#print( 'pixel:', x, ',', y, ' = ', r, g, b )
im.show()
im.save("html/images/colored_fence_odd.png")