import os
from http.server import HTTPServer, SimpleHTTPRequestHandler

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1111

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), SimpleHTTPRequestHandler)
print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )
SERVER.serve_forever()