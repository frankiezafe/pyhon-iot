# sudo -H python3 -m pip install setuptools
# sudo -H python3 -m pip install python-osc

import argparse
import random
import time
from pythonosc import osc_server
from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import BlockingOSCUDPServer

#OSC_IP = "127.0.0.1"
OSC_IP = "0.0.0.0" # receive from anywhere
OSC_PORT = 23000

def print_random(unused_addr, args, r):
	print("[{0}] ~ {1}".format(args[0], r))

def print_blob(unused_addr, args, *values):
	for blobi in range(0,values[0]):
		print( "blob " + str(blobi) + " pos: " + str(values[1+blobi*4]) + ":" + str(values[2+blobi*4]) + ", size: " + str(values[3+blobi*4]) + ":" + str(values[4+blobi*4]) )
	#print( values )
	#print("[{0}] ~ {1}".format(args[0], r))
	
dispatch = Dispatcher()
dispatch.map("/hello", print)
dispatch.map("/filter", print)
dispatch.map("/random", print_random, "Random")
dispatch.map("/blob", print_blob, "Blobs")

server = osc_server.ThreadingOSCUDPServer((OSC_IP, OSC_PORT), dispatch)
#server = BlockingOSCUDPServer((OSC_IP, OSC_PORT), dispatch)
print("Serving on {}".format(server.server_address))
server.serve_forever()