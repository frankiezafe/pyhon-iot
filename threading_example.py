import threading
from time import sleep

continue_loops = True

def keyboard():
	global continue_loops
	print('Enter your name:')
	x = input()
	print('Hello, ' + x)
	continue_loops = False

def toutletemps():
	while continue_loops:
		print( ".............................." )
		sleep( 0.1 )

def moinssouvent():
	while continue_loops:
		print( "##############################" )
		sleep( 1 )

thread_toutletemps = threading.Thread(target = toutletemps)
thread_toutletemps.daemon = True
thread_toutletemps.start()

thread_moinssouvent = threading.Thread(target = moinssouvent)
thread_moinssouvent.daemon = True
thread_moinssouvent.start()

thread_keyboard = threading.Thread(target = keyboard)
thread_keyboard.daemon = True
thread_keyboard.start()

while continue_loops:
	sleep( 1 )