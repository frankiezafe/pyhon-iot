#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#the next line is only needed for python2.x and not necessary for python3.x
from __future__ import print_function, division
import pygame
import random
import math

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GL.ARB.framebuffer_object import *
from OpenGL.GL.EXT.framebuffer_object import *

SCREEN_SIZE = (640,480)
PARTICLE_HISTORY = 0
PARTICLE_SUBDIV = 10

DARK_COLOR = (125,19,10)
LIGHT_COLOR = (255,180,120)
DISTANCE_MAX = 50
PARTICLE_SIZE_MIN = 3
PARTICLE_SIZE_MAX = 8

PAUSE = False

pygame.init()
screen=pygame.display.set_mode(SCREEN_SIZE, pygame.OPENGL|pygame.DOUBLEBUF)

clock = pygame.time.Clock()

mainloop = True
FPS = 30
playtime = 0.0

class Particle:
	
	_origin = None
	_pos = None
	_dir = None
	_force = None
	_velocity = None
	_deviation = None
	_nostalgy = None
	_history = None
	_dist_origin = None
	_color = None
	
	def v2_add(self, v0, v1):
		return [v0[0] + v1[0],v0[1] + v1[1]]
	
	def v2_minus(self, v0, v1):
		return [v0[0] - v1[0],v0[1] - v1[1]]
	
	def v2_length(self, v):
		return math.sqrt( v[0]*v[0] + v[1]*v[1] )
	
	def v2_dist(self, v0, v1):
		return self.v2_length( self.v2_minus(v0,v1) )
	
	def v2_mult(self, v0, f):
		return [v0[0] * f,v0[1] * f]
	
	def norm_dir(self):
		l = self.v2_length(self._dir)
		self._dir[0] /= l
		self._dir[1] /= l
	
	def __init__(self):
		self._origin = [ random.random() * SCREEN_SIZE[0], random.random() * SCREEN_SIZE[1] ]
		self._pos = [ self._origin[0], self._origin[1] ]
		self._dir = [ 0.5 - random.random(), 0.5 - random.random() ]
		self.norm_dir()
		self._force = [0,0]
		self._velocity = 1
		self._deviation = 0
		self._nostalgy = 0
		self._dist_origin = 0
		self._history = []
		self._color = [ random.random(), random.random(), random.random() ]
	
	def set_pos(self, x, y):
		self._origin = [x, y]
		self._pos = [x, y]
	
	def push( self, pos, strength, radius ):
		fdir = self.v2_minus( self._pos, pos )
		d = self.v2_length( fdir )
		if d > radius:
			return
		mult = 1 - ( d / radius )
		self._force = self.v2_add( self._force, self.v2_mult( fdir, strength * mult ) )
	
	def update(self, deltatime):

		global PARTICLE_HISTORY
		
		if PARTICLE_HISTORY > 0:
			self._history.append( (self._pos[0],self._pos[1]) )
			while len( self._history ) > PARTICLE_HISTORY:
				self._history.pop(0)
		
		if self._nostalgy != 0:
			pullback = self.v2_minus( self._origin, self._pos )
			self._force = self.v2_add( self._force, self.v2_mult( pullback, self._nostalgy * deltatime ) )
		
		self._pos = self.v2_add( self._pos, self.v2_mult( self._dir, self._velocity * deltatime ) )
		self._pos = self.v2_add( self._pos, self._force )
		self._force = [0,0]
		self._dist_origin = self.v2_dist( self._pos, self._origin )
		
		if self._pos[0] < 0 or self._pos[0] > SCREEN_SIZE[0]:
			if self._pos[0] < 0:
				self._pos[0] = 0
			if self._pos[0] > SCREEN_SIZE[0]:
				self._pos[0] = SCREEN_SIZE[0]
			self._dir[0] *= -1
		if self._pos[1] < 0 or self._pos[1] > SCREEN_SIZE[1]:
			if self._pos[1] < 0:
				self._pos[1] = 0
			if self._pos[1] > SCREEN_SIZE[1]:
				self._pos[1] = SCREEN_SIZE[1]
			self._dir[1] *= -1
		
		if self._deviation != 0:
			self._dir[0] += -self._deviation + random.random() * 2 * self._deviation
			self._dir[1] += -self._deviation + random.random() * 2 * self._deviation
			self.norm_dir()

class ParticleSystem:
	
	_particles = None
	_grid = None
		
	def __init__(self):
		self._particles = []
		self._grid = []
		for y in range( 0, SCREEN_SIZE[1] + 1, PARTICLE_SUBDIV ):
			line = []
			for x in range( 0, SCREEN_SIZE[0] + 1, PARTICLE_SUBDIV ):
				p = Particle()
				p.set_pos( x, y )
				self._particles.append( p )
				line.append( p )
			self._grid.append( line )
	
	def quad_vertices( self ):
		
		for y in range( 1, len(self._grid) ):
			
			l0 = self._grid[ y-1 ]
			l1 = self._grid[ y ]
			
			for x in range( 1, len(l0) ):

				c0 = l0[ x-1 ]
				c1 = l0[ x ]
				c2 = l1[ x ]
				c3 = l1[ x-1 ]

				glColor3f(c0._color[0],c0._color[1],c0._color[2]);
				glVertex3f(c0._pos[0],c0._pos[1], -c0._dist_origin);

				glColor3f(c1._color[0],c1._color[1],c1._color[2]);
				glVertex3f(c1._pos[0],c1._pos[1], -c1._dist_origin);

				glColor3f(c2._color[0],c2._color[1],c2._color[2]);
				glVertex3f(c2._pos[0],c2._pos[1], -c2._dist_origin);

				glColor3f(c3._color[0],c3._color[1],c3._color[2]);
				glVertex3f(c3._pos[0],c3._pos[1], -c3._dist_origin);
	
	def point_vertices(self):
		
		for p in self._particles:
			glVertex3f(p._pos[0],p._pos[1],0);
	
	def velocity(self, vel):
		for p in self._particles:
			p._velocity = vel
	
	def deviation(self, vel):
		for p in self._particles:
			p._deviation = vel
			
	def nostalgy(self, n):
		for p in self._particles:
			p._nostalgy = n
	
	def push( self, pos, strength, radius):
		for p in self._particles:
			p.push( pos, strength, radius )
	
	def update(self, deltatime):
		global pygame
		for p in self._particles:
			p.update(deltatime)

# pour faire une grille de particules, ne pas mettre d'arguments dans le constructeur
ps = ParticleSystem()
# pour les placer aléatoirement, mettre un nombre dans le constructeur
# ps = ParticleSystem(2000)
# vitesse des particules, en pixel/seconde
ps.velocity(0)
# changemment aléatoire de direction des particules
ps.deviation(0.01)
# tendance des particules à revenir à leur position initiale (s'oppose à la vélocité)
ps.nostalgy(0.15)

last_mouse_pos = pygame.mouse.get_pos()
mouse_radius = 0
mouse_radius_max = 100

t = 0

while mainloop:
	
	milliseconds = clock.tick(FPS)
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			mainloop = False 
		## KEYDOWN
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				mainloop = False
		elif event.type == pygame.KEYUP:
			## KEYUP
			if event.key == pygame.K_SPACE:
				PAUSE = not PAUSE
			pass
	
	if PAUSE:
		continue
	
	# UPDATE
	
	current_mouse_pos = pygame.mouse.get_pos()
	if last_mouse_pos != current_mouse_pos and mouse_radius < mouse_radius_max:
		mouse_radius += 50 * (milliseconds/1000.0)
		if mouse_radius > mouse_radius_max:
			mouse_radius = mouse_radius_max
	elif last_mouse_pos == current_mouse_pos and mouse_radius > 0:
		mouse_radius -= 90 * (milliseconds/1000.0)
		if mouse_radius < 0:
			mouse_radius = 0
	last_mouse_pos = current_mouse_pos
	
	if mouse_radius > 0:
		ps.push( current_mouse_pos, 0.1, mouse_radius )
		ps.push( (SCREEN_SIZE[0]-current_mouse_pos[0], SCREEN_SIZE[1]-current_mouse_pos[1]), 0.1, mouse_radius )
	
	ps.update( milliseconds/1000.0 )
	
	# RENDER
	
	glClearColor(0.0, 0.0, 0.0, 1.0)
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

	glDisable( GL_BLEND );
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_DEPTH_TEST );
	glMatrixMode(GL_PROJECTION);
	
	glLoadIdentity()
	
	gluPerspective(90,1,0.01,1000)
	'''
	gluLookAt(-1.4+math.sin(t/100.0)*2.5,
			   -1.4+math.sin(t/90.0)*0.4,
			   -1.4+math.sin(t/100.0+3.14)*0.5,
			   0,0,0,
			   0,1,0)
	'''
	gluLookAt( 
		SCREEN_SIZE[0]*0.5, SCREEN_SIZE[1]*0.5, -350,
		SCREEN_SIZE[0]*0.5, SCREEN_SIZE[1]*0.5, 0,
		0,-1,0)

	glMatrixMode(GL_MODELVIEW);
	
	glBegin(GL_QUADS);
	ps.quad_vertices()
	glEnd()
	glFlush()
	
	'''
	glColor3f( 1,1,1 );
	glBegin(GL_POINTS);
	ps.point_vertices();
	glEnd()
	glFlush()
	'''
	
	text = "FPS: {0:.2f}".format(clock.get_fps())
	pygame.display.set_caption(text)
	
	pygame.display.flip()

# Finish Pygame.  
pygame.quit()

# At the very last:
print("Particles was played for {0:.2f} seconds".format(playtime))

