#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
002_display_fps.py

Open a Pygame window and display framerate.
Program terminates by pressing the ESCAPE-Key.
 
works with python2.7 and python3.4 

URL	: http://thepythongamebook.com/en:part2:pygame:step002
Author : horst.jens@spielend-programmieren.at
License: GPL, see http://www.gnu.org/licenses/gpl.html
"""

#the next line is only needed for python2.x and not necessary for python3.x
from __future__ import print_function, division
import pygame

# Initialize Pygame.
pygame.init()
# Set size of pygame window.
screen=pygame.display.set_mode((640,480))
# Create empty pygame surface.
background = pygame.Surface(screen.get_size())
# Fill the background white color.
background.fill((255, 0, 255))
# Convert Surface object to make blitting faster.
background = background.convert()
# Copy background to screen (position (0, 0) is upper left corner).
screen.blit(background, (0,0))
# Create Pygame clock object.  
clock = pygame.time.Clock()

mainloop = True
# Desired framerate in frames per second. Try out other values.			  
FPS = 30
# How many seconds the "game" is played.
playtime = 0.0

# ----------- X -- Y
pos_ball = [ 320, 240 ]
# ---------------- UP - RIGHT - DOWN - LEFT
arrows_state = [ False, False, False, False ]

while mainloop:
	
	# Do not go faster than this framerate.
	milliseconds = clock.tick(FPS) 
	playtime += milliseconds / 1000.0 
	
	for event in pygame.event.get():
		# User presses QUIT-button.
		if event.type == pygame.QUIT:
			mainloop = False 
		## KEYDOWN
		elif event.type == pygame.KEYDOWN:
			# User presses ESCAPE-Key
			if event.key == pygame.K_ESCAPE:
				mainloop = False
			elif event.key == pygame.K_UP:
				arrows_state[0] = True
			elif event.key == pygame.K_DOWN:
				arrows_state[2] = True
			elif event.key == pygame.K_RIGHT:
				arrows_state[1] = True
			elif event.key == pygame.K_LEFT:
				arrows_state[3] = True
		elif event.type == pygame.KEYUP:
			## KEYUP
			if event.key == pygame.K_UP:
				arrows_state[0] = False
			elif event.key == pygame.K_DOWN:
				arrows_state[2] = False
			elif event.key == pygame.K_RIGHT:
				arrows_state[1] = False
			elif event.key == pygame.K_LEFT:
				arrows_state[3] = False
	
	if arrows_state[0]:
		pos_ball[1] -= 1
	if arrows_state[1]:
		pos_ball[0] += 1
	if arrows_state[2]:
		pos_ball[1] += 1
	if arrows_state[3]:
		pos_ball[0] -= 1
	
	# reset background
	background.fill((255, int(playtime*100)%255, 255))
	
	# drawing stuff
	pygame.draw.circle(background, (0,0,255), (pos_ball[0],pos_ball[1]),25)
	
	# update background
	screen.blit(background, (0,0))
	
	# Print framerate and playtime in titlebar.
	text = "FPS: {0:.2f}   Playtime: {1:.2f}".format(clock.get_fps(), playtime)
	pygame.display.set_caption(text)

	#Update Pygame display.
	pygame.display.flip()

# Finish Pygame.  
pygame.quit()

# At the very last:
print("This game was played for {0:.2f} seconds".format(playtime))

