#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#the next line is only needed for python2.x and not necessary for python3.x
from __future__ import print_function, division
import pygame
import random
import math

SCREEN_SIZE = (640,480)
PARTICLE_HISTORY = 10
PARTICLE_SUBDIV = 10

DARK_COLOR = (125,19,10)
LIGHT_COLOR = (255,180,120)
DISTANCE_MAX = 50
PARTICLE_SIZE_MIN = 3
PARTICLE_SIZE_MAX = 8

# set a number to randomise particles apparition
#PARTICLE_NUM = 1500
#VELOCITY = 50
#DEVIATION = 0.3
#NOSTALGY = 0
# set to None to display a grid of particles
PARTICLE_NUM = None
VELOCITY = 0
DEVIATION = 0
NOSTALGY = 0.6

# set to True if you want to stop inetraction if the mouse
# is not moving
MOUSE_CONTRACTION = False

PAUSE = False

pygame.init()
screen=pygame.display.set_mode(SCREEN_SIZE)
background = pygame.Surface(screen.get_size())
background.fill((0, 0, 0,255))
background = background.convert()
screen.blit(background, (0,0))
clock = pygame.time.Clock()

mainloop = True
FPS = 30
playtime = 0.0

class Particle:
	
	_origin = None
	_pos = None
	_dir = None
	_force = None
	_velocity = None
	_deviation = None
	_nostalgy = None
	_history = None
	_dist_origin = None
	
	def v2_add(self, v0, v1):
		return [v0[0] + v1[0],v0[1] + v1[1]]
	
	def v2_minus(self, v0, v1):
		return [v0[0] - v1[0],v0[1] - v1[1]]
	
	def v2_length(self, v):
		return math.sqrt( v[0]*v[0] + v[1]*v[1] )
	
	def v2_dist(self, v0, v1):
		return self.v2_length( self.v2_minus(v0,v1) )
	
	def v2_mult(self, v0, f):
		return [v0[0] * f,v0[1] * f]
	
	def norm_dir(self):
		l = self.v2_length(self._dir)
		self._dir[0] /= l
		self._dir[1] /= l
	
	def __init__(self):
		self._origin = [ random.random() * SCREEN_SIZE[0], random.random() * SCREEN_SIZE[1] ]
		self._pos = [ self._origin[0], self._origin[1] ]
		self._dir = [ 0.5 - random.random(), 0.5 - random.random() ]
		self.norm_dir()
		self._force = [0,0]
		self._velocity = 1
		self._deviation = 0
		self._nostalgy = 0
		self._dist_origin = 0
		self._history = []
	
	def set_pos(self, x, y):
		self._origin = [x, y]
		self._pos = [x, y]
	
	def push( self, pos, strength, radius ):
		fdir = self.v2_minus( self._pos, pos )
		d = self.v2_length( fdir )
		if d > radius:
			return
		mult = 1 - ( d / radius )
		self._force = self.v2_add( self._force, self.v2_mult( fdir, strength * mult ) )
	
	def update(self, deltatime):

		global PARTICLE_HISTORY
		
		if PARTICLE_HISTORY > 0:
			self._history.append( (self._pos[0],self._pos[1]) )
			while len( self._history ) > PARTICLE_HISTORY:
				self._history.pop(0)
		
		if self._nostalgy != 0:
			pullback = self.v2_minus( self._origin, self._pos )
			self._force = self.v2_add( self._force, self.v2_mult( pullback, self._nostalgy * deltatime ) )
		
		self._pos = self.v2_add( self._pos, self.v2_mult( self._dir, self._velocity * deltatime ) )
		self._pos = self.v2_add( self._pos, self._force )
		self._force = [0,0]
		self._dist_origin = self.v2_dist( self._pos, self._origin )
		
		if self._pos[0] < 0 or self._pos[0] > SCREEN_SIZE[0]:
			if self._pos[0] < 0:
				self._pos[0] = 0
			if self._pos[0] > SCREEN_SIZE[0]:
				self._pos[0] = SCREEN_SIZE[0]
			self._dir[0] *= -1
		if self._pos[1] < 0 or self._pos[1] > SCREEN_SIZE[1]:
			if self._pos[1] < 0:
				self._pos[1] = 0
			if self._pos[1] > SCREEN_SIZE[1]:
				self._pos[1] = SCREEN_SIZE[1]
			self._dir[1] *= -1
		
		if self._deviation != 0:
			self._dir[0] += -self._deviation + random.random() * 2 * self._deviation
			self._dir[1] += -self._deviation + random.random() * 2 * self._deviation
			self.norm_dir()

class ParticleSystem:
	
	_particles = None
	
	def __init__(self, num = None):
		if num == None:
			self._particles = []
			for y in range( 0, SCREEN_SIZE[1] + 1, PARTICLE_SUBDIV ):
				for x in range( 0, SCREEN_SIZE[0] + 1, PARTICLE_SUBDIV ):
					p = Particle()
					p.set_pos( x, y )
					self._particles.append( p )
		else:
			self._particles = [ Particle() for i in range(0,num) ]
	
	def velocity(self, vel):
		for p in self._particles:
			p._velocity = vel
	
	def deviation(self, vel):
		for p in self._particles:
			p._deviation = vel
			
	def nostalgy(self, n):
		for p in self._particles:
			p._nostalgy = n
	
	def push( self, pos, strength, radius):
		for p in self._particles:
			p.push( pos, strength, radius )
	
	def update(self, deltatime):
		global pygame
		for p in self._particles:
			p.update(deltatime)
			# mixing colors
			d = p._dist_origin / DISTANCE_MAX
			if d > 1:
				d = 1
			di = 1 - d
			c = ( DARK_COLOR[0]*di + LIGHT_COLOR[0]*d, DARK_COLOR[1]*di + LIGHT_COLOR[1]*d, DARK_COLOR[2]*di + LIGHT_COLOR[2]*d )
			pygame.draw.rect(background, c, (p._pos[0]-1.5,p._pos[1]-1.5,3,3))
			if PARTICLE_HISTORY > 0:
				if len(p._history) > 1:
					last_p = None
					for h in p._history:
						if last_p != None:
							pygame.draw.line(background, (255,255,255), last_p, h, 1)
						last_p = h

# pour faire une grille de particules, ne pas mettre d'arguments dans le constructeur
# ps = ParticleSystem()
# pour les placer aléatoirement, mettre un nombre dans le constructeur
ps = ParticleSystem(PARTICLE_NUM)
# vitesse des particules, en pixel/seconde
ps.velocity(VELOCITY)
# changemment aléatoire de direction des particules
ps.deviation(DEVIATION)
# tendance des particules à revenir à leur position initiale (s'oppose à la vélocité)
ps.nostalgy(NOSTALGY)

last_mouse_pos = pygame.mouse.get_pos()
mouse_radius = 0
mouse_radius_max = 100

while mainloop:
	
	milliseconds = clock.tick(FPS) 
	playtime += milliseconds / 1000.0 
	
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			mainloop = False 
		## KEYDOWN
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				mainloop = False
		elif event.type == pygame.KEYUP:
			## KEYUP
			if event.key == pygame.K_SPACE:
				PAUSE = not PAUSE
			pass
	
	if PAUSE:
		continue
	
	background.fill((0,0,0,255))
	
	current_mouse_pos = pygame.mouse.get_pos()
	if MOUSE_CONTRACTION:
		if last_mouse_pos != current_mouse_pos and mouse_radius < mouse_radius_max:
			mouse_radius += 50 * (milliseconds/1000.0)
			if mouse_radius > mouse_radius_max:
				mouse_radius = mouse_radius_max
		elif last_mouse_pos == current_mouse_pos and mouse_radius > 0:
			mouse_radius -= 90 * (milliseconds/1000.0)
			if mouse_radius < 0:
				mouse_radius = 0
		last_mouse_pos = current_mouse_pos

		# pour faire varier la force avec laquelle on pousse,
		# modifier le 2e argument
		# pour modifier la zon d'influence de la souris
		# modifier le 3e argument
		if mouse_radius > 0:
			ps.push( current_mouse_pos, 0.1, mouse_radius )
			ps.push( (SCREEN_SIZE[0]-current_mouse_pos[0], SCREEN_SIZE[1]-current_mouse_pos[1]), 0.1, mouse_radius )
			pygame.draw.circle(background, (0,180,180), current_mouse_pos, int(mouse_radius))

	else:
		ps.push( current_mouse_pos, 0.3, mouse_radius_max )
		ps.push( (SCREEN_SIZE[0]-current_mouse_pos[0], SCREEN_SIZE[1]-current_mouse_pos[1]), 0.3, mouse_radius_max )
		pygame.draw.circle(background, (0,180,180), current_mouse_pos, int(mouse_radius_max))
		
	ps.update( milliseconds/1000.0 )
	
	screen.blit(background, (0,0))
	
	text = "FPS: {0:.2f}   Playtime: {1:.2f}".format(clock.get_fps(), playtime)
	pygame.display.set_caption(text)

	pygame.display.flip()

# Finish Pygame.  
pygame.quit()

# At the very last:
print("Particles was played for {0:.2f} seconds".format(playtime))

