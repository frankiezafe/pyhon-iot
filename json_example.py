import json

# list of dicts
pydict = [
	{
		'id': 2345,
		'txt': 'some text'
	},
	{
		'id': 43,
		'txt': 'other text'
	},
	{
		'id': 45372,
		'txt': '<a href="ddd">\'OMG!\'</a>'
	}
]

# json conversion

jdata = json.dumps( pydict )
print('python > json',jdata)

# saving json
with open("tmp.json", "w") as write_file:
	json.dump( pydict, write_file )
	write_file.close()

# loading json
with open("tmp.json", "r") as read_file:
	data = json.load(read_file)
	print( 'json > python',data )
	read_file.close()