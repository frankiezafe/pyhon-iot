import os
import cgi
from http.server import HTTPServer, SimpleHTTPRequestHandler

HTTP_IP = '' # you can restrict the webserver to a specific IP, for security reasons
HTTP_PORT = 1111
UPLOAD_FOLDER = 'html/upload'

class Handler(SimpleHTTPRequestHandler):
	
	def get_head(self):
		return '''file upload<br/>
		<form enctype="multipart/form-data" method="POST">
		<input id="fileupload" name="file" type="file" />
		<input type="submit" value="submit" id="submit" />
		</form>'''
	
	def get_uploads(self):
		txt = ''
		for item in os.listdir(UPLOAD_FOLDER):
			if item[:1] == '.':
				continue
			path = os.path.join(UPLOAD_FOLDER, item)
			txt += '<li><a href="/' + path + '">' + item + '</a></li>'
		return '<ul>' + txt + '</ul>'
	
	def send_html(self, txt):
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.wfile.write(txt.encode("utf-8"))
	
	def show_file(self):
		
		mimetype = 'text/plain'
		
		if self.path.rfind('.jpg') > 0 or self.path.rfind('.jpeg') > 0:
			mimetype = "image/jpg"
		elif self.path.rfind('.png') > 0:
			mimetype = "image/png"
		elif self.path.rfind('.gif') > 0:
			mimetype = "image/gif"
		elif self.path.rfind('.svg') > 0:
			mimetype = "image/svg+xml"
		
		fi = str(self.path[1:])
		fi_size = os.stat(fi).st_size
		fi_data = open(fi, 'rb')			
		self.send_response(200)
		self.send_header("Content-type", mimetype)
		self.send_header("Content-length", fi_size)
		self.end_headers()
		self.wfile.write(fi_data.read())
		fi_data.close()
	
	def do_GET(self):
		
		if len(self.path) > 1:
			self.show_file()
			return
			
		self.send_html( self.get_head() + self.get_uploads() )	
		return
	
	def do_POST(self):
		
		form = cgi.FieldStorage(self.rfile, headers=self.headers, environ={'REQUEST_METHOD':'POST','CONTENT_TYPE':self.headers['Content-Type'],})
		
		if "file" in form:
			
			form_file = form['file']
			
			if form_file.filename:
				
				better_name = form_file.filename.lower().replace(' ','_')
				
				uploaded_file_path = os.path.join(UPLOAD_FOLDER, os.path.basename(better_name))
				
				with open(uploaded_file_path, 'wb') as fout:
					# read the file in chunks as long as there is data
					while True:
						chunk = form_file.file.read(100000)
						if not chunk:
							break
						# write the file content on a file on the hdd
						fout.write(chunk)
				
				html = self.get_head()
				html += '<b>file %s successfully saved on disk!</b><br/>' % ( uploaded_file_path )
				html += self.get_uploads()
				self.send_html( html )
				return
		
		self.send_html('something went awfully wrong...')

SERVER = HTTPServer((HTTP_IP, HTTP_PORT), Handler)

print( "running server on %s:%s" % ( HTTP_IP, HTTP_PORT ) )

SERVER.serve_forever()