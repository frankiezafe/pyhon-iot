import requests
from bs4 import BeautifulSoup

url = "https://twitter.com/nokia"
response = requests.get(url)
soup = BeautifulSoup(response.text, "html.parser")
tweets = soup.findAll('li',{"class":'js-stream-item'})
for tweet in tweets:
	if tweet.find('p',{"class":'tweet-text'}):
		tweet_user = tweet.find('span',{"class":'username'}).text.strip()
		tweet_text = tweet.find('p',{"class":'tweet-text'}).text.encode('utf8').strip()
		replies = tweet.find('span',{"class":"ProfileTweet-actionCount"}).text.strip()
		retweets = tweet.find('span', {"class" : "ProfileTweet-action--retweet"}).text.strip()
		print(tweet_user)
		print(tweet_text)
		print(replies)
		print(retweets)
	else:
		continue