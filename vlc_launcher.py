import subprocess

'''
pour lancer un programme dans le server X (interface graphique)
$ export DISPLAY=:0
source: https://superuser.com/questions/1035787/open-a-movie-with-vlc-from-command-line-via-ssh
'''

#cmd0 = "export DISPLAY=:0"
cmd0 = "killall vlc"
cmd1 = "vlc --fullscreen /home/pi/Desktop/iot/html/upload/treize_224p.mp4 "

subprocess.call(cmd0.split( ' ' ))
subprocess.Popen(cmd1.split( ' ' ))
print( "FINI" )