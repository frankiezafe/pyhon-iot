# sudo -H python3 -m pip install setuptools
# sudo -H python3 -m pip install python-osc

import argparse
import random
import time
from pythonosc import osc_message_builder
from pythonosc import udp_client

OSC_PORT = 23000

OSCclients = []
for i in range(2,256):
	c = {
		'ip' : "10.3.14."+str(i) + ":" + str(OSC_PORT),
		'osc' : udp_client.SimpleUDPClient("10.3.14."+str(i), OSC_PORT),
		'active' : True
	}
	OSCclients.append( c )

# discovering active OSCclients
for c in OSCclients:
	try:
		c['osc'].send_message("/hello", random.random())
		print( "Successfully contact " + c['ip'] )
	except:
		print( "Failed to contact " + c['ip'] )
		c['active'] = False

while True:
	for c in OSCclients:
		if not c['active']:
			continue
		c['osc'].send_message("/random", random.random())
	time.sleep(1)